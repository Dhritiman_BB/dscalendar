//
//  DayCollectionCell.swift
//  MyCal
//
//  Created by Dhritiman Saha on 07/11/17.
//  Copyright © 2017 Dhritiman Saha. All rights reserved.
//

import UIKit

class DayCollectionCell: UICollectionViewCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    
}
