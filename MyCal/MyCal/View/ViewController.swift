//
//  ViewController.swift
//  MyCal
//
//  Created by Dhritiman Saha on 01/11/17.
//  Copyright © 2017 Dhritiman Saha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var comp = NSDateComponents()
    @IBOutlet weak var monthYearPicker: UIPickerView!
    
    @IBOutlet weak var pickerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateDisplayLabel: UILabel!
    @IBOutlet weak var calendarCollectionVw: UICollectionView!
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    var titleDateFormater = DateFormatter()
    var dayFormater = DateFormatter()
    var selectedDateFormater = DateFormatter()
    var monthFormater = DateFormatter()
    var yearFormater = DateFormatter()
    
    var currentIndex = 0
    var currentDate = Date()
    var selectedDate = Date()
    var currentCalendar = Calendar.current
    var dayCountOnSelectedDate = 0
        
    var monthsArray = [String]()
    var yearsArray = NSMutableArray()
    
    var minimumYear: Int64 = 1970
    var maximumYear: Int64 = 2050
    
    var monthYearBtnTag = 0
    
    var isPickerOpen = false
    
    var daysGapToDisplay = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initialConfiguration()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initialConfiguration() {
        print(dayFormater.shortMonthSymbols)
        self.monthsArray = dayFormater.shortMonthSymbols
        for i in minimumYear ..< maximumYear+1 {
            self.yearsArray.add(i)
        }
//        print("YEAR ARR = \(self.yearsArray)")
        
        selectedDateFormater.dateFormat = "dd MM, yyyy"
        
        dayFormater.dateFormat = "EE"
        titleDateFormater.dateFormat = "MMM, YYYY"
        self.currentDate = Date()
        self.dateDisplayLabel.text = titleDateFormater.string(from: Date())
        // Calculate start and end of the current year (or month with `.month`):
        let interval = currentCalendar.dateInterval(of: .month, for: Date())!
        
        // Compute difference in days:
        dayCountOnSelectedDate = currentCalendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        print("DAY COUNT = \(dayCountOnSelectedDate)")
        daysGapToDisplay = extraDaysToDisplay(day: calculateFirstDayOfADate(forDate: Date()))
        dayCountOnSelectedDate = dayCountOnSelectedDate + daysGapToDisplay
        
        // Do any additional setup after loading the view, typically from a nib.
        
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: UIScreen.main.bounds.width/7, height: 50)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.calendarCollectionVw.collectionViewLayout = layout
    }

    func calculateFirstDayOfADate(forDate: Date) -> (String) {
        let components = currentCalendar.dateComponents([.year, .month], from: forDate)
        let startOfMonth = currentCalendar.date(from: components)
        print(dayFormater.string(from: startOfMonth!)) // 2015-11-01
        return dayFormater.string(from: startOfMonth!)
    }
    
    func dayMonthYearPopulate(date: Date) {
        self.currentDate = date
        titleDateFormater.dateFormat = "MMM, YYYY"
        self.dateDisplayLabel.text = titleDateFormater.string(from: date)
        // Calculate start and end of the current year (or month with `.month`):
        let interval = currentCalendar.dateInterval(of: .month, for: date)!
        
        // Compute difference in days:
        dayCountOnSelectedDate = currentCalendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        
        daysGapToDisplay = extraDaysToDisplay(day: calculateFirstDayOfADate(forDate: date))
        print("EXTRA DAY TO SHIFT = \(daysGapToDisplay)")
        dayCountOnSelectedDate = dayCountOnSelectedDate + daysGapToDisplay
        
        self.calendarCollectionVw.reloadData()
    }
    
    func extraDaysToDisplay(day: String) -> (Int){
        switch day.lowercased() {
        case "mon":
            return 1
        case "tue":
            return 2
        case "wed":
            return 3
        case "thu":
            return 4
        case "fri":
            return 5
        case "sat":
            return 6
        default:
            return 0
        }
    }
    
    @IBAction func previousButtonClicked(_ sender: Any) {
        currentIndex = currentIndex - 1
        dayMonthYearPopulate(date: currentCalendar.date(byAdding: .month, value: currentIndex, to: Date())!)
    }
    
    @IBAction func nextButtonClicked(_ sender: Any) {
        currentIndex = currentIndex + 1
        dayMonthYearPopulate(date: currentCalendar.date(byAdding: .month, value: currentIndex, to: Date())!)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dayCountOnSelectedDate
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: DayCollectionCell = (collectionView.dequeueReusableCell(withReuseIdentifier: "DayCollectionCell",
                                                                          for: indexPath) as! DayCollectionCell)
        
        cell.dayLabel.text = ((indexPath.row+1)-daysGapToDisplay)>0 ? "\((indexPath.row+1)-daysGapToDisplay)" : ""
        
        if cell.dayLabel.text != "" {
            if "\(cell.dayLabel.text!) \(self.currentCalendar.component(.month, from: self.currentDate)), \(self.currentCalendar.component(.year, from: self.currentDate))" == self.selectedDateFormater.string(from: Date()) {
                cell.backImageView.image = UIImage.init(named: "image1.png")
                cell.dayLabel.textColor = UIColor.white
            } else {
                cell.dayLabel.textColor = UIColor.white
                cell.backImageView.image = UIImage.init(named: "image2.png")
            }
        } else {
            cell.backImageView.image = nil
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/7, height: 50);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? DayCollectionCell {
            if selectedCell.dayLabel.text != "" {
                let selectedDateStr = "\(selectedCell.dayLabel.text!) \(self.currentCalendar.component(.month, from: self.currentDate)), \(self.currentCalendar.component(.year, from: self.currentDate))"
                self.selectedDate = self.selectedDateFormater.date(from: selectedDateStr)!
                let alert = UIAlertController.init(title: "Selected date", message: "\(self.selectedDateFormater.string(from: self.selectedDate))", preferredStyle: .alert)
                alert.addAction(UIAlertAction.init(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    @IBAction func selectMonthClicked(_ sender: UIButton) {
        self.monthYearBtnTag = sender.tag
        self.pickerHeightConstraint.constant = 202
        self.monthYearPicker.reloadAllComponents()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.monthFormater.dateFormat = "MMM"
        self.monthYearPicker.selectRow(self.monthsArray.index(of: self.monthFormater.string(from: Date()))!, inComponent: 0, animated: true)
    }
    
    @IBAction func selectYearClicked(_ sender: UIButton) {
        self.monthYearBtnTag = sender.tag
        self.pickerHeightConstraint.constant = 202
        self.monthYearPicker.reloadAllComponents()
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        self.yearFormater.dateFormat = "YYYY"
        self.monthYearPicker.selectRow(self.yearsArray.index(of: Int64(self.yearFormater.string(from: Date()))!), inComponent: 0, animated: true)
    }
    
    @IBAction func donePickerBtnClicked(_ sender: Any) {
        let dateRangeStart = currentDate
        let dateRangeEnd = Date().addingTimeInterval(12345678)
        let components = Calendar.current.dateComponents([.weekOfYear, .month], from: dateRangeStart, to: dateRangeEnd)
        self.pickerHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
}

extension ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return self.monthsArray.count
        default:
            return self.yearsArray.count
        }
//        switch self.monthYearBtnTag {
//        case 100:
//
//        default:
//
//        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch self.monthYearBtnTag {
        case 100:
            return "\(self.monthsArray[row])"
        default:
            return "\(self.yearsArray[row])"
        }
    }
}

